# Pablos

This is a project built on the technologies of Packer, Ansible, Bash and Linux OS.

The motivation around this project is to provide an automated build process for individuals or businesses to create custom SOE images for AWS.

This repo will eventually contain bootstrap scripts for automating OS security hardening in line with CIS benchmarks.

## Prerequisites

- Packer v1.4.5 (Other Versions may work however we use the latest for our project)
- Ansible v2.4+
- Bash 4.0
- Linux (RHEL Based of cause!)


## Installing
### Packer

```

wget https://releases.hashicorp.com/packer/1.4.5/packer_1.4.5_linux_amd64.zip

unzip packer_1.4.5_linux_amd64.zip

sudo mv packer /usr/local/bin/

# There is an issue with some RHEL based systems where the cracklib-packer binary is symlinked to packer
# To get around this you may need to execute the command below

unlink /usr/sbin/packer

```
Details of the bug are here:
https://github.com/cracklib/cracklib/issues/7


### Powershell Core

```
# Register the Microsoft signature key
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc

# Register the Microsoft RedHat repository
curl https://packages.microsoft.com/config/rhel/7/prod.repo | sudo tee /etc/yum.repos.d/microsoft.repo

# Update the list of products
sudo dnf update

# Install a system component
sudo dnf install compat-openssl10

# Install PowerShell
sudo dnf install -y powershell

# Start PowerShell
pwsh

```



### Ansible

```
# RHEL Installation
sudo dnf install ansible

# MacOS & Linux Installation
pip install --user ansible
```


## Authors

* **Hayden Smith** - *Initial work* - [Smiddie31](https://gitlab.com/smiddie31)
* **Martin Ozekin** - *Initial work* - [Mozekin](https://gitlab.com/mozekin)

## Acknowledgments

* The AWS Bear (aka bob)
* Ghandi
* Linus Torvalds
