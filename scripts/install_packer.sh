#!/bin/bash

source function

export PACKER_RELEASE="1.4.5"

cd /tmp/ || die "$@"
wget https://releases.hashicorp.com/packer/${PACKER_RELEASE}/packer_${PACKER_RELEASE}_linux_amd64.zip
unzip packer_${PACKER_RELEASE}_linux_amd64.zip
mv packer /usr/local/bin

if ! command -v packer >/dev/null 2>&1; then
  die
  exit 1
fi