#!/usr/bin/env bash

source functions

YUM_PACKAGES=(
wget unzip
)

if ! command -v yum >/dev/null 2>&1; then
  die
  exit 1
fi

yum clean metadata
yum update -y && yum check-update

if [ $? == 100 ]; then
    log "You've got updates available, installling now..."
    yum upgrade -y
else
    log "No updates pending..."
fi

yum install -y "${YUM_PACKAGES[@]}"
