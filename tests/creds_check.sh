#!/bin/bash

source ../functions

if ! env | grep AWS_ | grep -v BASH > /dev/null; then
  die "None found"
fi

export CREDS=/tmp/$$.tmp

for cred in $(env|grep AWS_ | grep -v BASH)
do
  log "export ${cred}" >> ${CREDS}
done

# output to stdout and clipboard
< ${CREDS} tee
# cleanup
rm ${CREDS}