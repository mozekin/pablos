#!/bin/bash

#
# Wrapper script to execute required bootstrap scripts located in scripts directory
#

#-----------------------------------------------
# Ensure we log stdout & stderr to bootstrap.log
#-----------------------------------------------
source functions

LOG="/var/log/bootstrap.log"
exec > >(tee -a ${LOG}); exec 2>&1

on_exit() {
  #-------------------------
  # Execute any cleanup code
  #-------------------------
  typeset RET=$?
  exit ${RET}
}
trap on_exit EXIT

#--------------
# START OF MAIN
#--------------
BASE=$(cd "$(dirname $0)" && pwd)
SCRIPTS=$(echo $(for i in ${*:-?} ; do ls ${BASE}/scripts/[install-]*-${i}*.sh; done))

log "$(date): Starting  execution of: \"$(basename "$0") ${*}\" [${SCRIPTS}]"

for SCRIPT in ${SCRIPTS} ; do
  log "$(date): Starting  execution of: \"${SCRIPT}\""
  echo "----------------------------------------------------------------------------"
  echo "Contents of: \"${SCRIPT}\""
  echo "----------------------------------------------------------------------------"
  cat "${SCRIPT}"
  echo "----------------------------------------------------------------------------"

  ${SCRIPT}; RET=$?

  log "$(date): Completed execution of: \"${SCRIPT}\" [${RET}]"
  [[ ${RET} -ne 0 ]] && (( FAILED+=1 ))
done

log "$(date): Completed execution of: \"$(basename "$0") ${*}\""

log "$(date): Writing log file ${LOG} to /dev/console"
cat ${LOG} >/dev/console

exit ${FAILED:-0}
