help:           ## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/:[^#]*##/:\t/'

verify.env: ##  Test build environment dependencies
	ansible-playbook -i "all," ansible/playbooks/verify.env.yml